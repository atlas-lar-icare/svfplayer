```bash
   ______    ________         __                     
  / ___/ |  / / ____/  ____  / /___ ___  _____  _____
  \__ \| | / / /_     / __ \/ / __ `/ / / / _ \/ ___/
 ___/ /| |/ / __/    / /_/ / / /_/ / /_/ /  __/ /    
/____/ |___/_/      / .___/_/\__,_/\__, /\___/_/     
                   /_/            /____/             
```

# Overview

This project is a fork of guido_svfplayer (https://gitlab.in2p3.fr/fcrescio/guido_svfplayer) repository written by Francesco Crescioli which implements a XVC ( Xilinx Virtual Cable) SVF Parser/Player.

# Key Features

- Provide a Fast XVC protocol based on XVC 1.0 communication protocol

  Extension of the *shift* message, this command is used between the client and the server to transfer low-level JTAG vectors.

| Syntax                                           | Comment                                                   |
| ------------------------------------------------ | --------------------------------------------------------- |
| "shift:`<num bits><tms vector><tdi vector>`" | Requests are standard TMS/TDI with TDO capture cycles     |
| "**shift;**`<num bits><tms vector><tdi vector>`" | Requests are TMS/TDI cycles without TDO capture and reply |

# Compiling

```bash
% mkdir build
% cd build
% cmake ..
% cmake --build .
```

# Usage

```bash
svfplayer -H|--host <XVC server> -f|--svffile <filename> [OPTIONS...]:
  -H [ --host ] arg     XVC Server host
  -f [ --svffile ] arg  SVF file with commands
  Options:
    -h [ --help ]       produce help message
    -v [ --verbose ]    Changes the verbosity level during the parsing
    -N [ --nocomment ]  Suppress the print of the comments found in the SVF
                        file
    -S [ --standard ]   Use the standard version of the XVC protocol
```
