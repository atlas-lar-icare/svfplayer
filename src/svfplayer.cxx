/**
 *
 * SVF player, designed to parse SVF file and send the JTAG command
 * sequences through XVC 
 *
 * 2016 - francesco.crescioli@lpnhe.in2p3.fr
 * 2023 - fatih.bellachia@lapp.in2p3.fr
 *
 * based on AMB/LAMB code by Guido Volpi
 */

#include <sys/time.h>

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/asio.hpp>
#include <boost/array.hpp>

#include <exception>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <cstring>
#include <cstddef>
#include <iostream>
#include <cwchar>
#include <clocale>
#include <csignal>

static const char *description = {
  "SVF player, designed to parse SVF file and send the JTAG command\n" \
  "sequences through XVC (based on AMB/LAMB code by Guido Volpi).\n\n" \
  "Contributors:\n" \
  "  - Guido Volpi\n" \
  "  - Francesco Crescioli\n" \
  "  - Fatih Bellachia\n\n" \
  "Report bugs to <fatih.bellachia@lapp.in2p3.fr>\n\n" \
  "Usage: svfplayer -H|--host <XVC server> -f|--svffile <filename> [OPTIONS...]"
};

using namespace std;

/** This class represent a JTAG sequence, handling the
 * buffer that contain the TMS, TDI and TDO signals.
 * The size of the buffers is the same by default and can
 * be dynamically set.
 */
class JTAGCmdBuffer {
private:
	string _name;

  size_t _nbits;
  size_t _npos;

  bool _hastdo; // true if the TDO was set

  bool _moveexit;

  unsigned char *_tms;
  unsigned char *_tdi;
  unsigned char *_smask;
  unsigned char *_tdo;
  unsigned char *_mask;

public:
  /** Construct the buffer */
  JTAGCmdBuffer(const char *name = "") : _name(name), _nbits(0), _npos(0),
  _hastdo(false), _moveexit(false),
  _tms(0x0), _tdi(0x0), _smask(0x0), _tdo(0x0), _mask(0x0)
  {
    // nothing to be done
  }

  /** Destructor */
  ~JTAGCmdBuffer() {
    if (_nbits) {
      delete [] _tms;
      delete [] _tdi;
      delete [] _smask;
      delete [] _tdo;
      delete [] _mask;
    }
  }

  JTAGCmdBuffer(const JTAGCmdBuffer &cpy) :
  	_name(cpy._name), _nbits(cpy._nbits), _npos(cpy._npos),
  	_hastdo(cpy._hastdo), _moveexit(cpy._moveexit)
  {
  	if (!_nbits) return;

  	_tms = new unsigned char[_npos];
  	_tdi = new unsigned char[_npos];
  	_smask = new unsigned char[_npos];
  	_tdo = new unsigned char[_npos];
  	_mask = new unsigned char[_npos];

  	for (size_t pos=0; pos!=_npos; ++pos) {
  		_tms[pos] = cpy._tms[pos];
  		_tdi[pos] = cpy._tdi[pos];
  		_smask[pos] = cpy._smask[pos];
  		_tdo[pos] = cpy._tdo[pos];
  		_mask[pos] = cpy._mask[pos];
  	}
  }


  /** Set the size, as number of bits, resizing
   * all the buffers, resetting all the parameters.
   *
   * If the new size is equal to the previous the fields
   * are reset, excluding MASK and SMASK.
   * Compatible with: http://www.xilinx.com/support/answers/3173.html
   * */
  void setSize(size_t n) {
    _hastdo = false;
    _moveexit = false;

    if (n!=_nbits) {
      // need to resize all, first clearing the memory
      if (_nbits) {
        delete [] _tms;
        delete [] _tdi;
        delete [] _smask;
        delete [] _tdo;
        delete [] _mask;
      }

      // resize the internal structures, evaluating its size
      _nbits = n;
      _npos = (n+7)/8;

      if (_nbits==0) return; // skip

      _tms = new unsigned char[_npos];
      _tdi = new unsigned char[_npos];
      _mask = new unsigned char[_npos];
      _tdo = new unsigned char[_npos];
      _smask = new unsigned char[_npos];


      // reset all the elements and fill the masks
      for (size_t pos=0; pos!=_npos; ++pos) {
        _tms[pos] = 0x0;
        _tdi[pos] = 0x0;
        _mask[pos] = 0xff;
        _tdo[pos] = 0x0;
        _smask[pos] = 0xff;
      }
      // position 0 represents the first digit, sometime is not full
      if (_nbits&0x7) _mask[0] = _smask[0] = ~((~0)<<(_nbits&0x7));
    }
    else {
      // reset all the elements
      for (size_t pos=0; pos!=_npos; ++pos) {
        _tms[pos] = 0x0;
        _tdi[pos] = 0x0;
        _tdo[pos] = 0x0;
      }
    }
  }

  /** Return the name of SVF command */
  string getName() const { return _name; }

  /** Return the number of bits */
  inline size_t getNBits() const { return _nbits; }
  /** Return the number of words in the buffers */
  inline size_t getBufferSize() const { return _npos; }

  /** Return true if the TDO was set of this buffer */
  bool hasTDO() const { return _hastdo; }
  /** Check that the command has a TDO to be checked */
  void checkTDO() { _hastdo = true; }

  // Control the stream of TMS bits
  void setTMS(size_t pos, unsigned char val) { _tms[pos] = val; }
  unsigned char getTMS(size_t pos) const { return _tms[pos]; }
  unsigned char* getTMS() { return _tms; }

  void setTDI(size_t pos, unsigned char val) { _tdi[pos] = val; }
  unsigned char getTDI(size_t pos) const { return _tdi[pos]; }
  unsigned char* getTDI() const { return _tdi; }

  void setSMask(size_t pos, unsigned char val) { _smask[pos] = val; }
  unsigned char getSMask(size_t pos) const { return _smask[pos]; }
  unsigned char* getSMask() const { return _smask; }

  void setTDO(size_t pos, unsigned char val) { _tdo[pos] = val;}
  unsigned char getTDO(size_t pos) const { return _tdo[pos]; }
  unsigned char* getTDO() const { return _tdo; }

  void setMask(size_t pos, unsigned char val) { _mask[pos] = val; }
  unsigned char getMask(size_t pos) const { return _mask[pos]; }
  unsigned char* getMask() const { return _mask; }

  /** This method specify that this represents the final sequence
   * of a stream of commands, this implies that the Exit flag is
   * set and the latest TMS bit to be sent has to be 0
   */
  void setExit() {
    _moveexit = true;
    _tms[0] |= 1<<((_nbits-1)&7); // set the last bit to move, usually used for shift-*r
  }

  bool getExit() const { return _moveexit; }

  /** Print a status summary of all the buffers. The first line represents the
   * number of bits and positions in the buffer (in paranthesis).
   *
   * The following lines are the exact status for TMS, TDI, SMASK, TDO, MASK.
   *
   * A tag parameter, prepended to all the lines can be set.
   *  */
  void printStatus(const char *tag="") {
    cout << tag << _name << " #b (#B): " << _nbits << "(" << _npos << ")" << endl;
    cout << tag << _name << " TMS  :" << hex;
    for (size_t pos=0; pos!=_npos; ++pos) cout << setfill('0') << setw(2) <<(unsigned int) _tms[pos];
    cout << dec << endl;
    cout << tag << _name << " TDI  :" << hex;
    for (size_t pos=0; pos!=_npos; ++pos) cout << setfill('0') << setw(2) << (unsigned int) _tdi[pos];
    cout << dec << endl;
    cout << tag << _name << " SMASK:" << hex;
    for (size_t pos=0; pos!=_npos; ++pos) cout << setfill('0') << setw(2) << (unsigned int) _smask[pos];
    cout << dec << endl;
    if (_hastdo) {
    	cout << tag << _name << " TDO  :" << hex;
    	for (size_t pos=0; pos!=_npos; ++pos) cout << setfill('0') << setw(2) << (unsigned int) _tdo[pos];
    	cout << dec << endl;
    	cout << tag << _name << " MASK :" << hex;
    	for (size_t pos=0; pos!=_npos; ++pos) cout << setfill('0') << setw(2) << (unsigned int) _mask[pos];
    	cout << dec << endl;
    }
  }

  /** The method compares the expected TDO status for the buffer with a buffer
   * collected externally.
   *
   * @param tdores a buffer of received TDOs, it has to be of the same size of the
   *  other buffers within the object.
   * @return true if the two buffers are identical, excluding bits set as "don't care"
   *  by the MASK
   */
  bool compareTDO(const unsigned char *tdores) const {
  	for (size_t pos=0; pos!=_npos; ++pos) {
  		if ((_tdo[pos]&_mask[pos])!=(tdores[pos]&_mask[pos])) return false;
  	}

  	return true;
  }
};

/** The class mirrors the Xilinx tap controller for FPGA programming
 * and provides function to move from a state another, returning the
 * set of JTAG commands to be gave.
 *
 * Objects of this class not directly control the FPGA TAP controller.
 *
 * TODO: the moveToState() method currently doesn't support all possible
 * transition, however looks to be covering most of the ones required
 * by FPGA programming, including flash memories.
 */
class JTAGTapControl {
public:
  // enumerate the possible states for the TAP controller
  enum TapState {Unkown=0, TestLogicReset, RunTestIDLE,
    SelectDRScan=10, CaptureDR, ShiftDR, Exit1DR, PauseDR, Exit2DR, UpdateDR,
    SelectIRScan=20, CaptureIR, ShiftIR, Exit1IR, PauseIR, Exit2IR, UpdateIR
  };

  /** Helper function used to convert a string representing a state in
   * the value according the enum TapState declaration.
   *
   * TODO Currently not all state are mapped, only the most used ones
   *
   * @param state, string representing the state name
   * @return the enum TapState corrspondent value, Unknown if not mapped
   */
  static TapState toTapState(string state) {
    if (state.find("RESET")!=string::npos) return TestLogicReset;
    else if (state.find("IDLE")!=string::npos) return RunTestIDLE;
    else if (state.find("DRSELECT")!=string::npos) return SelectDRScan;
    else if (state.find("DRCAPTURE")!=string::npos) return CaptureDR;
    else if (state.find("DRSHIFT")!=string::npos) return ShiftDR;
    else if (state.find("DREXIT1")!=string::npos) return Exit1DR;
    else if (state.find("DRPAUSE")!=string::npos) return PauseDR;
    else if (state.find("DREXIT2")!=string::npos) return Exit2DR;
    else if (state.find("DRUPDATE")!=string::npos) return UpdateDR;
    else if (state.find("IRSELECT")!=string::npos) return SelectIRScan;
    else if (state.find("IRCAPTURE")!=string::npos) return CaptureIR;
    else if (state.find("IRSHIFT")!=string::npos) return ShiftIR;
    else if (state.find("IREXIT1")!=string::npos) return Exit1IR;
    else if (state.find("IRPAUSE")!=string::npos) return PauseIR;
    else if (state.find("IREXIT2")!=string::npos) return Exit2IR;
    else if (state.find("IRUPDATE")!=string::npos) return UpdateIR;
    else return Unkown;
  }

  static bool validRunState(string state) {
    if ((state.find("RESET")!=string::npos) ||
        (state.find("IDLE")!=string::npos) ||
        (state.find("DRPAUSE")!=string::npos) ||
        (state.find("IRPAUSE")!=string::npos))
      return true;
    else
      return false;
  }

  static const char* toName(TapState state) {
  	switch (state) {
  	case TestLogicReset:
  		return "RESET";
  	case RunTestIDLE:
  		return "IDLE";
    case SelectDRScan:
  		return "DRSELECT";
    case CaptureDR:
  		return "DRCAPTURE";
    case ShiftDR:
  		return "DRSHIFT";
    case Exit1DR:
  		return "DREXIT1";
    case PauseDR:
  		return "DRPAUSE";
    case Exit2DR:
  		return "DREXIT2";
    case UpdateDR:
  		return "DRUPDATE";
    case SelectIRScan:
  		return "IRSELECT";
    case CaptureIR:
  		return "IRCAPTURE";
    case ShiftIR:
  		return "IRSHIFT";
    case Exit1IR:
  		return "IREXIT1";
    case PauseIR:
  		return "IRPAUSE";
    case Exit2IR:
  		return "IREXIT2";
    case UpdateIR:
  		return "IRUPDATE";
  	default:
  		return "UNKOWN";
  	}
  }

private:
  TapState _curstate; ///< Current status for the TAP controller

public:
  JTAGTapControl() : _curstate(JTAGTapControl::RunTestIDLE)
  {;}

  /** Return the status of the TAP controller */
  TapState getState() const { return _curstate;  }
  /** Set the current status */
  void setState(TapState state) { _curstate = state; }

  /** Return the sequence of JTAG commands, usually all TMS commands,
   * required to move from the current state to a specific new state.
   *
   * @param newState, the new state
   * @return true if the transition is possible and current state
   *  is updated, if false the commands will allow to move to RESET and
   *  the internal state will reproduce this.
   */
  bool moveToState(TapState newState, JTAGCmdBuffer &buffer) {
    if (newState==TestLogicReset) {
      // five transition always go to TestLogicReset
    	_curstate = TestLogicReset;
      buffer.setSize(5);
      buffer.setTMS(0, 0x1f);
      return true;
    }
    else if (newState==_curstate) {
    	// nothing to be done
    	buffer.setSize(0);
    	return true;
    }

    // prepare the variables to move to the wanted state
    unsigned int tmsseq(0);
    int steps(0);
    bool onTargetState(false);

    if (_curstate==TestLogicReset) {
      // when here move from Reset to any known state
      switch (newState) {
        case RunTestIDLE:
          steps += 1;
          tmsseq = (tmsseq<<1) | 0x0;
          onTargetState = true;
          break;
        case ShiftDR:
          steps += 4;
          tmsseq = (tmsseq<<4) | 0x2;
          onTargetState = true;
          break;
        case ShiftIR:
          steps += 5;
          tmsseq = (tmsseq<<5) | 0x6;
          onTargetState = true;
          break;
        default:
          cerr << toName(_curstate)
               << " -> "
               << toName(newState)
               << " : Unknown transition, remain in RESET"
               << endl;
          newState = TestLogicReset;
          steps += 5;
          tmsseq = (tmsseq<<5) | 0x1f;
          onTargetState = false;
          break;
      }
    }
    else if (_curstate==RunTestIDLE) {
    	switch (newState) {
    	case ShiftDR:
    		steps += 3;
    		tmsseq = (tmsseq<<3) | 0x1;
    		onTargetState = true;
    		break;
    	case ShiftIR:
    		steps += 4;
    		tmsseq = (tmsseq<<4) | 0x3;
    		onTargetState = true;
    		break;
    	default:
        cerr << toName(_curstate)
             << " -> "
             << toName(newState)
             << " : Unknown transition, remain in RESET"
             << endl;
    		newState = TestLogicReset;
        steps += 5;
        tmsseq = (tmsseq<<5) | 0x1f;
    		onTargetState = false;
    		break;
    	}
    }
  	else if (_curstate==ShiftDR) {
  		switch (newState) {
  		case RunTestIDLE:
  			steps += 3;
  			tmsseq = (tmsseq<<3) | 0x3;
  			onTargetState = true;
  			break;
  		default:
        cerr << toName(_curstate)
             << " -> "
             << toName(newState)
             << " : Unknown transition, remain in RESET"
             << endl;
  			newState = TestLogicReset;
  			steps += 5;
  			tmsseq = (tmsseq<<5) | 0x1f;
  			onTargetState = false;
  			break;
  		}
  	}
  	else if (_curstate==ShiftIR) {
  		switch (newState) {
  		case RunTestIDLE:
  			steps += 3;
  			tmsseq = (tmsseq<<3) | 0x3;
  			onTargetState = true;
  			break;
  		default:
        cerr << toName(_curstate)
             << " -> "
             << toName(newState)
             << " : Unknown transition, remain in RESET"
             << endl;
  			newState = TestLogicReset;
  			steps += 5;
  			tmsseq = (tmsseq<<5) | 0x1f;
  			onTargetState = false;
  			break;
  		}
  	}
  	else if (_curstate==Exit1IR) {
  		switch (newState) {
  		case RunTestIDLE:
        steps += 2;
        tmsseq =(tmsseq<<2) | 0x1;
        onTargetState = true;
        break;
      case PauseIR:
        steps += 1;
        tmsseq =(tmsseq<<1) | 0x0;
        onTargetState = true;
        break;
  		default:
        cerr << toName(_curstate)
             << " -> "
             << toName(newState)
             << " : Unknown transition, remain in RESET"
             << endl;
  			newState = TestLogicReset;
  			steps += 5;
  			tmsseq = (tmsseq<<5) | 0x1f;
  			onTargetState = false;
  			break;
      }
  	}
  	else if (_curstate==Exit1DR) {
  		switch (newState) {
  		case RunTestIDLE:
        steps += 2;
        tmsseq =(tmsseq<<2) | 0x1;
        onTargetState = true;
        break;
  		default:
        cerr << toName(_curstate)
             << " -> "
             << toName(newState)
             << " : Unknown transition, remain in RESET"
             << endl;
  			newState = TestLogicReset;
  			steps += 5;
  			tmsseq = (tmsseq<<5) | 0x1f;
  			onTargetState = false;
  			break;
      }
  	}
  	else if (_curstate==PauseIR) {
  		switch (newState) {
  		case RunTestIDLE:
        steps += 3;
        tmsseq =(tmsseq<<3) | 0x3;
        onTargetState = true;
        break;
  		default:
        cerr << toName(_curstate)
             << " -> "
             << toName(newState)
             << " : Unknown transition, remain in RESET"
             << endl;
  			newState = TestLogicReset;
  			steps += 5;
  			tmsseq = (tmsseq<<5) | 0x1f;
  			onTargetState = false;
  			break;
      }
  	}
  	else {
      cerr << toName(_curstate)
           << " -> "
           << toName(newState)
           << " : Unknown transition, remain in RESET"
           << endl;
			newState = TestLogicReset;
			steps += 5;
			tmsseq = (tmsseq<<5) | 0x1f;
			onTargetState = false;
  	}


    /* Update the internal status and build the command buffer used
     * to send the commands used to move the real TAP controller.
     */
    _curstate = newState;
    buffer.setSize(steps);
    for (unsigned int i=0;i!=buffer.getBufferSize();++i) {
      buffer.setTMS(i,(tmsseq>>(i*8))&0xff);
    }

    return onTargetState;
  }

  /** Helper function that takes the newstate as string.
   *
   * */
  bool moveToState(string newState, JTAGCmdBuffer &buffer) {
    return moveToState(toTapState(newState), buffer);
  }

  /** This provides the steps required to run a test, remaining
   * in the current state for n cycles.
   */
  bool runTest(unsigned int nTCK, JTAGCmdBuffer &buffer) {
  	// the buffer will have the size of the number of clocks
  	// this incidentally also imply that nTCK bits in TMS are 0
  	buffer.setSize(nTCK); //
  	return true;
  }
};


class SVFInterpreterError : public exception {
public:
	const char *what() { throw std::runtime_error("Error in SVF file"); };
};

/** The class provide a generic SVF interpreter. The interpreter has a
 * send_command() method that by default just provides debugging information
 * to control that an SVF file has been parsed correctly. To actually
 * send a stream of JTAG command to a real device the class has to be
 * inherited and the send_command() method overload to reflect the
 * needs of the specific implementation.
 *
 */
class SVFInterpreter {
protected:
	int _verbose; // verbosity level
	bool _printComment; // regulate if the comments are reported or not

	float _frequency;

	bool _hasCable; // it is true if a real JTAG cable or similar connection exists

	size_t _totnbits; // total number of bits send to the JTAG chain
	unsigned _nTDOerrors;
	unsigned _nTDOchecks;
	double _TotIDLETime;
	unsigned int _TotIDLETck;

	string _filepath;

  ifstream _svffile;

	streampos _beginpos;
	streampos _endpos;

	JTAGTapControl _tapcontrol;

	// buffers used to store the JTAG commands during the parsing
	JTAGCmdBuffer _movbuffer;
	JTAGCmdBuffer _sirbuffer;
	JTAGCmdBuffer _sdrbuffer;
	JTAGCmdBuffer _hirbuffer;
	JTAGCmdBuffer _hdrbuffer;
	JTAGCmdBuffer _tirbuffer;
	JTAGCmdBuffer _tdrbuffer;
	JTAGCmdBuffer *_curbuffer; // buffer used in the current set of instructions

	JTAGTapControl::TapState _defir;
	JTAGTapControl::TapState _defdr;

	/** small function that converts a single character representing
	 * a hex value as integer.
	 */
	unsigned int _Char2Int(const char &c) {
	  if (c>='0' && c<='9') return c-'0';
	  else if (c>='a' && c<='f') return c-'a'+10;
	  else if (c>='A' && c<='F') return c-'A'+10;
	  else return 0;
	}

	virtual void send_command(const JTAGCmdBuffer &);
	virtual void flush_command();
	bool checkTDOs(const JTAGCmdBuffer &, const unsigned char *);

	void parse_bitmask(JTAGCmdBuffer *, unsigned int);

	string nextToken();

public:
	SVFInterpreter();
	virtual ~SVFInterpreter();

	void setVerbose(int verb) { _verbose = verb; }
	int getVerbose() const { return _verbose; }

	void setPrintComment(bool doit=true) { _printComment = doit; }
	bool getPrintComment() const { return _printComment; }

	bool hasCable() { return _hasCable; }

	unsigned int getNTDOErrors() const { return _nTDOerrors; }
	unsigned int getNTDOChecks() const { return _nTDOchecks; }

	void openFile(const char*);

	void parse();

	void printSummary();
};

SVFInterpreter::SVFInterpreter() :
		_verbose(0), _printComment(true),
		_frequency(1e6), _hasCable(false),
		_beginpos(0L), _endpos(0), _nTDOerrors(0),
		_tapcontrol(),
		_movbuffer("MOV"),
		_sirbuffer("SIR"), _sdrbuffer("SDR"),
		_hirbuffer("SIR"), _hdrbuffer("SDR"),
		_tirbuffer("SIR"), _tdrbuffer("SDR"),
		_curbuffer(0x0),
		_defir(JTAGTapControl::RunTestIDLE),
		_defdr(JTAGTapControl::RunTestIDLE)
{;}

SVFInterpreter::~SVFInterpreter()
{	; }

/** Open a file, throw an error in case of problems */
void SVFInterpreter::openFile(const char *fname) {
	_filepath = fname;

  _svffile.open(_filepath.c_str());

  if (_svffile.is_open()) {
    // check the file size, moving to the EOF and checking the position
    _beginpos = _svffile.tellg();
    _svffile.seekg(0, _svffile.end);
    _endpos = _svffile.tellg();
    _svffile.seekg(0, _svffile.beg);
  }
  else {
    ostringstream oss;

    oss << _filepath
        << ": No such file or Permission denied"
        << ", stopping the procedure.";

		throw ios_base::failure(oss.str());
	}
}

/** Internal function used to read the next word.
 *
 * The function read until a blank or ; symbol, the ; is
 * read only if is the only character before a blank/newline.
 */
string SVFInterpreter::nextToken() {
  string token;

  _svffile >> token;

  if (token==";") return token;
  else if (token[token.length()-1]==';') {
	 token.erase(token.length()-1);
    _svffile.putback(';');
  }

  return token;
}

/** Asks to parse the current file.
 *
 * The parsing is based on SVF files generated by Impact and may need
 * generaliziotion. */
void SVFInterpreter::parse() {
	// command parsing variables
	bool cmdbegin(true);
	string cmdword;
	string cmdarg;

	unsigned int oldfrac(0);
	struct timeval oldTime;
	gettimeofday(&oldTime, 0x0);

	while (1) { // SVF file parser loop

	  // check the position in the file to show that the read is not stuck
		streampos curpos = _svffile.tellg();
		unsigned int curfrac =100./(_endpos-_beginpos)*(curpos-_beginpos);
		struct timeval curTime;
		if (curfrac>oldfrac) {
		  gettimeofday(&curTime, 0x0);
		  cout << "Advance " << curfrac << "%" << " read " << curpos << " of " << _endpos << endl;
		  cout << "Time to process: " << curTime.tv_sec - oldTime.tv_sec << endl;
		  printSummary();
		  oldfrac = curfrac;
		  oldTime = curTime;
		}

	  // read the next string value
		string keyword(nextToken());
		//svffile >> keyword;

		if (_svffile.eof()) break; // end of the read loop

		if (cmdbegin) { // check if the line is a comment
			if ((keyword.find("//")==0)||(keyword.find("!")==0)) {
				getline(_svffile, cmdarg);
				/* The comments often help to identify the where the program is during the
				 * procedure and understand where problems happened, they are indeed
				 * usually printed, this can be suppressed.
				 */
				if (_printComment) cout << "Comment: " << cmdarg << endl;
				continue;
			}
			// if not a comment, this is the current command
			cmdword = keyword;
			cmdbegin = false; // the command is now started
		} // end check of the comment

		if (keyword==";") { // end of the command
			if (_verbose) cout << "+++ Execute: " << cmdword << " +++" << endl;

			// for a few commands there are actions that are taken
			if (cmdword=="SIR") { // Send data to the Instruction Register
			  // obtain the sequnce of command to move the ShiftIR position
			  if (!_tapcontrol.moveToState(JTAGTapControl::ShiftIR, _movbuffer)) {
			  	cerr << "[" << __LINE__ << "] Error: impossible to move to the Shift-IR state" << endl;
			  }
			  /* The final step that has to be loaded in the IR has to be identified
			   * because responsible of moving to to Exit1IR state, this final
			   * step can the last bit of the TIR or SIR, if TIR size is 0
			   */
			  if (_tirbuffer.getNBits()) _tirbuffer.setExit();
			  else _sirbuffer.setExit();

			  // Move to the ShiftIR position
			  if (_verbose) _movbuffer.printStatus();
			  send_command(_movbuffer);
			  // send the header
			  if (_verbose) _hirbuffer.printStatus();
			  send_command(_hirbuffer);
			  // send the instruction
			  if (_verbose) _sirbuffer.printStatus();
			  send_command(_sirbuffer);
			  // send the trailer
			  if (_verbose) _tirbuffer.printStatus();
			  send_command(_tirbuffer);
			  // comunicate the TAP representation the new state
			  _tapcontrol.setState(JTAGTapControl::Exit1IR);
			  // obtain the command to move to the default state
			  if (!_tapcontrol.moveToState(_defir, _movbuffer)) {
			  	cerr << "[" << __LINE__ << "] Error: impossible to move to the " << JTAGTapControl::toName(_defir) << " state" << endl;
			  }
			  if (_verbose) _movbuffer.printStatus();
			  send_command(_movbuffer);
			}
			else if (cmdword=="SDR") {
			  /* Move the TAP controller in the ShiftDR state and
			   * send data, similarly to what was done befor for the SIR
			   */
			  if (!_tapcontrol.moveToState(JTAGTapControl::ShiftDR, _movbuffer)) {
			  	cerr << "[" << __LINE__ << "] Error: impossible to move to the ShiftDR state" << endl;
			  }
			  if (_tdrbuffer.getNBits()) _tdrbuffer.setExit();
			  else _sdrbuffer.setExit();
			  if (_verbose) _movbuffer.printStatus();
			  send_command(_movbuffer);
			  if (_verbose) _hdrbuffer.printStatus();
			  send_command(_hdrbuffer);
			  if (_verbose) _sdrbuffer.printStatus();
			  send_command(_sdrbuffer);
			  if (_verbose) _tdrbuffer.printStatus();
			  send_command(_tdrbuffer);
			  _tapcontrol.setState(JTAGTapControl::Exit1DR);
			  if (!_tapcontrol.moveToState(_defdr, _movbuffer)) {
			  	cerr << "[" << __LINE__ << "] Error: impossible to move to the " << JTAGTapControl::toName(_defdr) << " state" << endl;
			  }
			  if (_verbose) _movbuffer.printStatus();
			  send_command(_movbuffer);
			}

      cmdbegin = true;
		}
		else if (keyword=="STATE") {
		  /* Move to a specific state in the TAP controller.
		   *
		   */

		  // read the state
		  _svffile >> cmdarg;

		  // move to the state, no success control is performed
		  if (_verbose) cout << "Move to: " << cmdarg << endl;
		  if (!_tapcontrol.moveToState(cmdarg, _movbuffer)) {
		  	cerr << "[" << __LINE__ << "] Error: impossible to move to the " << cmdarg << " state" << endl;
		  }
		  send_command(_movbuffer);

		  cmdbegin = true; // reset parser
		  continue;
		}
		else if (keyword=="ENDIR") {
		  // Update the TAP state to go after a SIR command
		  _svffile >> cmdarg;
		  _defir = JTAGTapControl::toTapState(cmdarg);

		  cmdbegin = true; // reset parser
		  continue;
		}
		else if (keyword=="ENDDR") {
		  // Update the TAP state to go after a SDR command
		  _svffile >> cmdarg;
		  _defdr = JTAGTapControl::toTapState(cmdarg);

		  cmdbegin = true; // reset parser
		  continue;
		}
		else if (keyword=="FREQUENCY") {
		  // read the cable frequency
		  string unit;
		  _svffile >> _frequency >> unit; // assumed to be Hz
		  cerr << "FREQUENCY command is read but later ignored, fixed at 1 MHz" << endl;
		  cmdbegin = true; // reset parser
		}
		else if (keyword=="RUNTEST") {
		  /* This command will put the TAP controller in IDLE state
		   * for some time. At the moment the number of clock or the
		   * seconds are interpreted as the amount of time the TAP
		   * controller should be in this state, no JTAG commands are
		   * sent.
		   * Official documentation of Xilinx has been found not particularly
		   * precise on this topic, however this was found to be workign fine
		   * and good to reduce the VME communication.
		   * TODO: The supported command line is probably not the most generic
		   * possible, more checks should performed.
		   */

		  // Prepare some variables and read the next 2 keywords, expected
		  // to be a delay value and time unit:
		  // expected syntax: RUNTEST VAL UNIT
		  string rawvalue;
		  string state("IDLE");
		  string unit;
      string endStateOption;
      string endState;
		  _svffile >> rawvalue >> unit;

		  // check the rawvalue does not represents a valid state,  in
		  // this case the expected syntax is:
		  // RUNTEST RUN_STATE VAL UNIT
		  if (JTAGTapControl::validRunState(rawvalue)==true) {
		    // reshuffle the variable to agree with the expected syntax
		    state = rawvalue;
		    rawvalue = unit;
		    _svffile >> unit; // unit has still to be read, taking from the file
		  }

		  // obtain the sequence of JTAG commands to move to the wanted state
		  // and send the command to the chain
		  if (_verbose) cout << "Move to: " << state << endl;
		  _tapcontrol.moveToState(state, _movbuffer);
		  send_command(_movbuffer);

		  if (unit.find("TCK")!=string::npos) {
		    /* If the unit is TCK, the delay is calculated as function of the
		     * frequency. First convert the rawvalue string in tick numbers.
		     */
		    char *pEnd = NULL;
		    unsigned int count = strtol(rawvalue.c_str(), &pEnd, 10);
		    unsigned int countthr(1000000);

		    if (count<countthr) {
		    	_TotIDLETck += count;
		    	_tapcontrol.runTest(count, _movbuffer);
		    	send_command(_movbuffer);
		    	count = 0;
		    }
		    else {
		    	_TotIDLETck += countthr;
		    	_tapcontrol.runTest(countthr, _movbuffer);
		    	send_command(_movbuffer);
		    	count -= countthr;
		    }

		    if (count>0) {
		    	// calculate the seconds and nanoseconds to be in this state
		    	unsigned int nanosec = (count%1000000)*1000;
		    	unsigned int sec = count/1000000;
		    	_TotIDLETime += sec+nanosec*1e-9;
		    	timespec deltaT = {sec, nanosec};
		    	if (_verbose)  cout << "Sleep a little: (" << sec << ", " << nanosec << ")" << endl;
		    	// the sleep is ignored if this is in test mode.
		    	if (_hasCable) nanosleep(&deltaT,0x0);
		    }
		  }
		  else if (unit.find("SEC")!=string::npos) {
		    // The time to wait in the given state is expressed in seconds,
		    // given as floating point number
        char *pEnd = NULL;
        float sectime = strtof(rawvalue.c_str(), &pEnd);

		    _TotIDLETime += sectime;

		    // Extract the number of seconds and nanoseconds fir the timespect structure
		    unsigned int nanosec = sectime*1e9;
		    unsigned int sec = sectime;
		    nanosec -= sec*1000000000;

		    timespec deltaT = {sec, nanosec};
		    if (_verbose)  cout << "Sleep a little: (" << sec << ", " << nanosec << ")" << endl;
		    if (_hasCable) nanosleep(&deltaT,0x0); // do nothing if in test mode
		  }
		  else {
		  	cerr << "Units not recognized: " << keyword << " " << rawvalue << " " << unit << endl;
		  }

		  // check the next word does not represents a valid option, in
		  // this case the expected syntax is:
		  // ENDSTATE end_state
      char buffer[256];

      _svffile.getline(buffer, sizeof buffer);

      istringstream iss(buffer);

      // When an end_state is specified, it becomes the default.
      // When a run_state is not specified, the default end_state
      // remains in effect.
		  if (iss.str().find("ENDSTATE")!=string::npos) {
        iss >> endStateOption >> endState;
        size_t pos;

        if ((pos = endState.rfind(';'))!=string::npos)
          endState.erase(pos);

        if (JTAGTapControl::validRunState(endState)==true) {
          // When a run_state is specified, the new run_state becomes
          // the default end_state.
          if (state == rawvalue)
            endState = state;
        }
      }
      else {
        // If the end_state is not specified, the default end state is used.
        // The initial default for endState is IDLE.
        endState = "IDLE";
      }

      // If the test bus is already in the end state,
      // no state transitions occur.
      if (state != endState) {
        if (_verbose) cout << "Move to: " << endState << endl;
        _tapcontrol.moveToState(endState, _movbuffer);
        send_command(_movbuffer);
      }

		  cmdbegin = true; // reset parser
		}
		else if (keyword=="SIR") {
		  /* In this case all the following keywords, until ;
		   * will be referred to a SIR command.
		   */
		  cmdword = keyword;
		  size_t nbits;
		  _svffile >> nbits;
		  _sirbuffer.setSize(nbits);
		  _curbuffer = &_sirbuffer;
		}
		else if (keyword=="SDR") {
      /* In this case all the following keywords, until ;
       * will be referred to a SDR command.
       */
		  cmdword = keyword;
		  size_t nbits;
		  _svffile >> nbits;
		  _sdrbuffer.setSize(nbits);
		  _curbuffer = &_sdrbuffer;
		}
		else if (keyword=="HIR") {
      /* In this case all the following keywords, until ;
       * will be referred to a HIR command.
       */
		  cmdword = keyword;
		  size_t nbits;
		  _svffile >> nbits;
		  _hirbuffer.setSize(nbits);
		  _curbuffer = &_hirbuffer;
		}
		else if (keyword=="TIR") {
      /* In this case all the following keywords, until ;
       * will be referred to a TIR command.
       */
		  cmdword = keyword;
		  size_t nbits;
		  _svffile >> nbits;
		  _tirbuffer.setSize(nbits);
		  _curbuffer = &_tirbuffer;
		}
		else if (keyword=="HDR") {
      /* In this case all the following keywords, until ;
       * will be referred to a HDR command.
       */
		  cmdword = keyword;
		  size_t nbits;
		  _svffile >> nbits;
		  _hdrbuffer.setSize(nbits);
		  _curbuffer = &_hdrbuffer;
		}
		else if (keyword=="TDR") {
		  /* In this case all the following keywords, until ;
		   * will be referred to a TDR command.
		   */
		  cmdword = keyword;
		  size_t nbits;
		  _svffile >> nbits;
		  _tdrbuffer.setSize(nbits);
		  _curbuffer = &_tdrbuffer;
		}
		else if (keyword=="TDI") {
		  /* Set the TDI value for the command */
      parse_bitmask(_curbuffer, 0);
		}
		else if (keyword=="SMASK") {
		  /* Update the SMASK value for the current command */
		  parse_bitmask(_curbuffer, 1);
		}
		else if (keyword=="TDO") {
		  /* Set the TDO message expected as output of this command,
		   * the checkTDO() command is required to enable the check
		   * of the value.
		   */
		  parse_bitmask(_curbuffer, 2);
		  _curbuffer->checkTDO();
		}
		else if (keyword=="MASK") {
		  /* update the MASK status of the current command, usually SDR,
		   * using the value from the file.
		   */
		  parse_bitmask(_curbuffer, 3);
		}
		else if (keyword=="TRST") {
		  _svffile >> cmdarg; // take the value
		  cout << "TRST is ignored, was required " << cmdarg << endl;
		}
		else {
		  // The command was unknown, this not considered blocking but
		  // a message will be printed out
		  if (cmdbegin) cout << "?command " << keyword << endl;
		  else cout << "?argument " << keyword << endl;
		}
 	} // end SVF file parser loop

	flush_command();
}

bool is_not_hexa(char c) {
	return !isxdigit(c);
}

void SVFInterpreter::parse_bitmask(JTAGCmdBuffer *cmdbuffer, unsigned int id) {
	bool numstart(false);
	char c1, c2;
	unsigned char *buffer(0x0); // point the the specific buffer

	if (id==0) buffer = cmdbuffer->getTDI();
	else if (id==1) buffer = cmdbuffer->getSMask();
	else if (id==2) buffer = cmdbuffer->getTDO();
	else if (id==3) buffer = cmdbuffer->getMask();

	size_t pos(0);

	string str;
	int idx;
	int start_idx;

  // Find first "("
	numstart = false;
	while(1) {
		_svffile >> c1;
		if (c1=='(') {
			numstart = true;
			break;
		}
		if (c1==')' or (c1==';')) {
			break;
		}
	}
	if(numstart == false) {
		cerr << "*** possible bitmask parsing error" << endl;
		return;
	}

  // Get String From first "(" to first ")"
	getline(_svffile, str, ')');
	str.erase(remove_if(str.begin(), str.end(), is_not_hexa), str.end());

  // Handle odd number of character
	start_idx = 0;
	if((str.length() % 2) != 0) {
		buffer[pos] = _Char2Int(str[0]);
		pos++;
		start_idx = 1;
	}

  // Parse all others characters
	for(idx=start_idx; idx<str.length(); idx+=2) {
		if (pos==cmdbuffer->getBufferSize()) {
			cerr << "Incorrect size of the buffer, writing position " << pos << ", size is " << cmdbuffer->getBufferSize() << endl;
		}

		buffer[pos] = (_Char2Int(str[idx])<<4) | _Char2Int(str[idx+1]);
		pos++;
	}
}

void SVFInterpreter::printSummary() {
	cout << "Total number of bits: " << _totnbits << endl;
	cout << "Total number of TDO errors: " << _nTDOerrors <<  " (Tot checks " << _nTDOchecks << ")" << endl;
	cout << "Total RUNTEST IDLE time " << _TotIDLETime << " secs, Tcks " << _TotIDLETck << endl;
}

void SVFInterpreter::send_command(const JTAGCmdBuffer &cmd) {
	// sum the number of bits
	_totnbits += cmd.getNBits();
}

void SVFInterpreter::flush_command() {
}

bool SVFInterpreter::checkTDOs(const JTAGCmdBuffer &cmd, const unsigned char *tdores) {
	bool checkRes = cmd.compareTDO(tdores);

	_nTDOchecks += 1;
	_nTDOerrors += !checkRes;

	if (_verbose || (!checkRes && _hasCable)) {
		cout << "Check: " << (checkRes? "OK" : "Failed") << " --> (#elements = " << cmd.getBufferSize() << ")"<< endl;
		const unsigned int nrowels(16);
		for (unsigned int irow=0; irow!=(cmd.getBufferSize()+nrowels-1)/nrowels; ++irow) {
			for (size_t pos=0; pos!=nrowels; ++pos) {
				const unsigned int gpos(pos+irow*nrowels);
				if (gpos<=cmd.getBufferSize())
					cout << setfill('0') << setw(2) << hex << (unsigned int) (tdores[gpos]&cmd.getMask(gpos));
				else cout << "  ";
			}
			cout << "    ";
			for (size_t pos=0; pos!=nrowels; ++pos) {
				const unsigned int gpos(pos+irow*nrowels);
				if (gpos<=cmd.getBufferSize())
					cout << setfill('0') << setw(2) << hex << (unsigned int) (cmd.getTDO(gpos)&cmd.getMask(gpos));
				else cout << "  ";
			}
			cout << dec << endl;
		}
	}

	return checkRes;
}



/** Implementation of the SVFIntepreter designed to meet
 * the needs for JTAG comunnication via XVC
 */
class SVFInterpreterXVC : public SVFInterpreter {
private:
  string _host;
  unsigned int _serverBufSize;
  boost::asio::ip::tcp::socket _socket;
  boost::array<char, 8192*4> _buf;
  unsigned int _multiCmdCycles;
  bool _standardXVC;

  void send_command(const JTAGCmdBuffer &);
  void flush_command();
  void print_progress(const string& cmd, double percentage);

public:
  SVFInterpreterXVC(boost::asio::io_service& io_service, const string& server);
  void setStandardXVC(bool value) { _standardXVC = value; }
  int getStandardXVC() const { return _standardXVC; }
  void close(void);

  virtual ~SVFInterpreterXVC();
};

SVFInterpreterXVC::SVFInterpreterXVC(boost::asio::io_service& io_service, const string& host):
  _host(host), _socket(io_service)
{
  boost::asio::ip::tcp::resolver resolver(io_service);
  boost::asio::ip::tcp::resolver::query query(boost::asio::ip::tcp::v4(), _host, "2542");
  boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
  boost::asio::ip::tcp::resolver::iterator end;
  boost::asio::ip::tcp::no_delay option(true);
  boost::system::error_code error = boost::asio::error::host_not_found;

  while (error && (iterator != end)) {
    _socket.close();
    cout << "Try to connect to XVC server @ " << iterator->endpoint() << endl;
    _socket.connect(*iterator++, error);
  }

  if(error)
    throw boost::system::system_error(error);

  _socket.set_option(option);

  boost::array<char, 20> buf;
  std::string message = "getinfo:";
  std::copy(message.begin(),message.end(),buf.begin());  

  _socket.write_some(boost::asio::buffer(buf, 8), error);

  if(error)
    throw boost::system::system_error(error);

  memset(buf.data(), 0, buf.size());

  _socket.read_some(boost::asio::buffer(buf,20), error);

  if(error)
    throw boost::system::system_error(error);

  _serverBufSize = atoi(&buf[15])/2;
  cout << "Server buffer size: " << _serverBufSize << endl;
  _multiCmdCycles = 0;
  _standardXVC = false;
}

SVFInterpreterXVC::~SVFInterpreterXVC() {
  close();
}

void SVFInterpreterXVC::close(void) {
  _socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
  _socket.close();
}

void SVFInterpreterXVC::flush_command() {
  boost::system::error_code error;
  unsigned int shiftSize = ((_multiCmdCycles+7)/8);
  boost::array<char, 10> buf;
  cout << "Flush: "<< _multiCmdCycles << endl;
  if (_multiCmdCycles > 0) {
    std::string message = "shift;";
    std::copy(message.begin(),message.end(),buf.begin());   
    std::memcpy(&buf[6], &_multiCmdCycles, sizeof(unsigned int));
    _socket.write_some(boost::asio::buffer(buf, 10), error);

    if(error)
      throw boost::system::system_error(error);

    _socket.write_some(boost::asio::buffer(_buf, shiftSize*2), error);

    if(error)
      throw boost::system::system_error(error);

    //if (_verbose)
    cout << "Sent " << _multiCmdCycles << "/" << _multiCmdCycles << " cycles in " << shiftSize << " bytes " << endl;
    //_socket.read_some(boost::asio::buffer(_buf,shiftSize));
    _multiCmdCycles = 0;
  }
}

void SVFInterpreterXVC::print_progress(const string& cmd, double percentage)
{
  const char *pbstr = "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||";
  vector<string> spinner_circle;
  size_t len = strlen(pbstr);
  size_t lpad = (size_t) (percentage * len);
  size_t rpad = len - lpad;
  unsigned int value = (unsigned int) (percentage * 100);
  static unsigned char idx = 0;

  spinner_circle.push_back("◐");
  spinner_circle.push_back("◓");
  spinner_circle.push_back("◑");
  spinner_circle.push_back("◒");

  printf("\r%3s command %3u%% [%.*s%*s] ",
          cmd.c_str(), value, lpad, pbstr, rpad, "");
  fflush (stdout);
  cout << spinner_circle[idx] << flush;

  idx = (idx + 1) % spinner_circle.size();
}

/** The send command method is designed to meet the design of
 * the JTAG control through XVC for the IPMC mezzanine of the DF boards.
 */
void SVFInterpreterXVC::send_command(const JTAGCmdBuffer &cmd) {
  _totnbits += cmd.getNBits();

  bool bigCommand(cmd.getNBits()>1000000);

  bool pauseStream = false;
  
  unsigned int bufOffset = 0;

  boost::system::error_code error;

  unsigned char *tdores = new unsigned char[cmd.getBufferSize()];
  for (size_t pos=0; pos!=cmd.getBufferSize(); ++pos) tdores[pos] = 0;

  // now we can proceed with a single command send and check
  for (size_t ibit=0; ibit<cmd.getNBits();) {
    if (ibit%(1024*10)==1)
      pauseStream = true;

    unsigned int shiftSize = cmd.getBufferSize()-bufOffset > _serverBufSize ? _serverBufSize : cmd.getBufferSize()-bufOffset;
    unsigned int cycles = cmd.getNBits() - ibit > shiftSize*8 ? shiftSize*8 : cmd.getNBits() - ibit;

    if (cmd.hasTDO() or _standardXVC or pauseStream) {
      std::string message = "shift:";
      std::copy(message.begin(),message.end(),_buf.begin());
    } else {
      std::string message = "shift;";
      std::copy(message.begin(),message.end(),_buf.begin());
    }

    std::memcpy(&_buf[6], &cycles, sizeof(unsigned int));

    _socket.write_some(boost::asio::buffer(_buf, 10), error);

    if(error)
      throw boost::system::system_error(error);
    
    for (int i = 0; i < shiftSize; i++) {
      _buf[i] = cmd.getTMS(cmd.getBufferSize()-i-bufOffset-1);
      _buf[shiftSize+i] = cmd.getTDI(cmd.getBufferSize()-i-bufOffset-1);
    }

    _socket.write_some(boost::asio::buffer(_buf, shiftSize*2), error);

    if(error)
      throw boost::system::system_error(error);
    
    print_progress(cmd.getName(), (double)(cycles+ibit)/cmd.getNBits());

    if (cmd.hasTDO() or _standardXVC or pauseStream) 
      _socket.read_some(boost::asio::buffer(_buf,shiftSize));
    for (int i = 0; i < shiftSize; i++) {
      tdores[cmd.getBufferSize()-1-i-bufOffset] = _buf[i];
    }
    bufOffset+=shiftSize;
    ibit+=shiftSize*8;
    if (pauseStream)
      pauseStream = false;
  }

  cout << endl;

  if (cmd.hasTDO())
    checkTDOs(cmd, tdores);

  delete [] tdores;
}

SVFInterpreter *svfplayer(NULL);

void handler(int signum)
{
  cerr << endl << "svfplayer: Receive signal " << strsignal(signum) << endl;
  delete svfplayer;

  exit(signum);
}

int main(int argc, char **argv)
{
  using namespace  boost::program_options ;

  int verbose(0);

  signal(SIGINT, handler);
  signal(SIGQUIT, handler);
  signal(SIGTERM, handler);

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc(description);
  desc.add_options()
    ("help,h", "produce help message")
    ("host,H", value<string>(), "XVC Server host")
    ("svffile,f", value<string>(), "SVF file with commands")
    ("verbose,v", "Changes the verbosity level during the parsing")
    ("nocomment,N", "Suppress the print of the comments found in the SVF file")
    ("standard,S", "Use the standard version of the XVC protocol")
    ;

  variables_map vm;

  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);
  }
  catch(...) // In case of errors during the parsing process, desc is printed for help
    {
      cerr << argv[0] << ": Failure in command_line_parser." << endl;
      return 1;
    }

  if(vm.count("help")) // if help is required, then desc is printed to output
    {
      cout << desc;
      return 0;
    }

  if(vm.count("svffile") == 0)
    {
      cerr << "svfplayer: You must specify `--svffile <filename>' and `--host <XVC server>' options" << endl
           << "Try `svfplayer --help' for more information." << endl;
      return 1;
    }

  if(vm.count("host") == 0)
    {
      cerr << "svfplayer: You must specify `--svffile <filename>' and `--host <XVC server>' options" << endl
           << "Try `svfplayer --help' for more information." << endl;
      return 1;
    }

  setlocale(LC_CTYPE, "en.US.UTF-8");

  string svffile_path = vm["svffile"].as<string>();
  bool printComment = vm.count("nocomment")==0;

  struct timeval startTime;
  gettimeofday(&startTime, 0x0);

  int res = 0;
  boost::asio::io_service ios;

  // implement an interpreter compatible with the options
  try {
    SVFInterpreterXVC *svfplayerxvc;

    svfplayer = new SVFInterpreterXVC(ios,vm["host"].as<string>());
    svfplayerxvc = dynamic_cast<SVFInterpreterXVC *>(svfplayer);

    if (vm.count("standard"))
      svfplayerxvc->setStandardXVC(true);
    else
      svfplayerxvc->setStandardXVC(false);

    // setup verbosity message levels
    if (vm.count("verbose"))
      svfplayer->setVerbose(1);
    else
      svfplayer->setVerbose(0);

    svfplayer->setPrintComment(printComment);

    // open the file and parse it
    svfplayer->openFile(svffile_path.c_str());
    svfplayer->parse();

    if (!svfplayer->getNTDOErrors()) {
      cout << "ALL GOOD" << endl;
    }
    else { 
      cout << "Possible issues..." << endl;
      svfplayer->printSummary();
    }

    delete svfplayer;

    struct timeval endTime;
    gettimeofday(&endTime, 0x0);

    cout << endl << "Elapsed time: " << endTime.tv_sec-startTime.tv_sec << " seconds" << endl;
  }
  catch (std::exception& ex) {
    cerr << argv[0] << ": [Exception] " << ex.what() << endl;
    delete svfplayer;
  }

  return res;
}
